 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="../../index3.html" class="brand-link">
         <img src="<?= base_url('assets/vendor') ?>/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
         <span class="brand-text font-weight-light">AdminLTE 3</span>
     </a>

     <!-- Sidebar -->
     <div class="sidebar">
         <!-- Sidebar user (optional) -->
         <div class="user-panel mt-3 pb-3 mb-3 d-flex">
             <div class="image">
                 <img src="<?= base_url('assets/vendor') ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
             </div>
             <div class="info">
                 <a href="#" class="d-block">Alexander Pierce</a>
             </div>
         </div>

         <!-- SidebarSearch Form -->
         <div class="form-inline">
             <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                 <div class="input-group-append">
                     <button class="btn btn-sidebar">
                         <i class="fas fa-search fa-fw"></i>
                     </button>
                 </div>
             </div>
         </div>

         <?php
            $role_id = $this->session->userdata('role_id');
            $queryMenu = "SELECT a.id_modul, a.nama_modul, a.icon_modul, a.url_modul, a.seq FROM modul as a JOIN user_access_modul as b ON a.id_modul = b.modul_id WHERE b.role_id = $role_id ORDER BY a.seq ASC";
            $menu = $this->db->query($queryMenu)->result_array();
            // var_dump($menu);
            // die;
            ?>

         <!-- Sidebar Menu -->
         <nav class="mt-2">
             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                 <?php
                    foreach ($menu as $m) { ?>
                     <li class="nav-item">
                         <a href="<?= site_url($m['url_modul']) ?>" class="nav-link">
                             <i class="nav-icon <?= $m['icon_modul'] ?>"></i>
                             <p>
                                <?= $m['nama_modul']; ?>
                             </p>
                         </a>
                     </li>
                 <?php } ?>

                 <li class="nav-item">
                     <a href="../widgets.html" class="nav-link">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Widgets
                             <span class="right badge badge-danger">New</span>
                         </p>
                     </a>
                 </li>

                 <li class="nav-item">
                     <a href="<?= base_url('auth/logout') ?>" class="nav-link">
                        <i class="fas fa-sign-out-alt"></i>
                         <p>
                             Logout
                         </p>
                     </a>
                 </li>
             </ul>
         </nav>
         <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
 </aside>

 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <div class="content-header">
         <div class="container">
             <!-- <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> Top Navigation <small>Example 3.0</small></h1>
                </div>
            </div> -->
         </div><!-- /.container-fluid -->
     </div>
     <!-- /.content-header -->